#include "translate.h"

float translate (int ft, int inch)
{
	return ((ft*12) + inch) * 2.54;
}