﻿#include "translate.h"


//   α[°] = α[рад] × (180° / π) или α[рад] × (360° / 2π);
//
//  α[рад] = α[°] : (180° / π) = α[°] × (π / 180°), 

double DegreeToRad(double angle)
{

	return angle*(180.0/3.1415926); // Лениво мне константу искать. Не забываем все сделать double для исключения ошибки приведения типов
}									// и ошибок округления

double RadToDegree(double angle)
{
	return angle*(3.1415926/180.0);
}

double translate(double angle, int flag)
{
	switch ( flag )
	{
	default:
		break;

	case 1:
		return DegreeToRad(angle);
		break;

	case 2:
		return RadToDegree(angle);
		break;
	}
	return angle;
}