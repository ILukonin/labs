// task_3.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdio.h"
#include "translate.h"
#include "input.h"


int main()
{
	float angle = 0;
	int flag = 0;

	flag = getangle( "Input angle. Format: 00.00R for radian or 00.00D for degree \n", &angle );

	if ( flag == 0 )
	{
		printf("Invalid angle!\n");
		return 1;
	}

	angle = translate(angle, flag);

	flag == 1 ? printf("%2.2fD\n",angle):printf("%2.2fR\n",angle) ;

	return 0;
}

