﻿// task_1.c: определяет точку входа для консольного приложения.
//

#include <stdio.h>
#include "input.h"
#include "test.h"


int main()
{
	enum ERROR  {OK, INVALID_VEIGHT, INVALID_SIZE, INVALID_SEX};
	enum SEX {MALE = 1, FEMALE};

	int veight = 0;
	int size = 0;
	int sex = 0;

	veight = getint("Input veight\n");
	if (veight == 0 )
	{
		printf("veight is too small or invalid!\n");
		return INVALID_VEIGHT;
	}

	size = getint("Input size\n");
	if (size == 0 )
	{
		printf("size is too small or invalid!\n");
		return INVALID_SIZE;
	}

	sex = getsex("Input sex\n");
	if (sex == 0 )
	{
		printf("sex is invalid!\n");
		return INVALID_SEX;
	}

	printf("your veight is %d\n",veight); //всегда надо писать спецификатор!
	printf("your size is %d\n",size);
	sex == MALE ? printf("your sex is male\n") : printf("your sex is female\n");

	switch (test(veight,size,sex))
	{
		default:
			printf("Something wrong\n");
			break;
		case 3:
			printf("You have to get fat\n");
			break;
		case 2:
			printf("Your veight is normal\n");
			break;
		case 1:
			printf("You need to lose weight\n");
			break;
	}




	return OK;
}

