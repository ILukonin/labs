#include "test.h"

int test_format(int hour, int min, int sec)
{
	if( (hour > 25)||( hour < 0))
	{
		return 1;
	}

	if( (min > 59)||( min < 0))
	{
		return 2;
	}

	if( (sec > 59)||( sec < 0))
	{
		return 3;
	}

	return 0;
}

int test (int hour, int min, int sec)
{

	enum DAYTIME { MORNING, AFTERNOON, EVENING, NIGHT, NOON, MIDNIGHT };
	
	switch ( test_format(hour, min, sec) )
	{
	default:
		printf("Bad time!\n");
		return -1;
		break;

	case 0:
		
		break;

	case 1:
		printf("Bad Hours!\n");
		return -1;
		break;

	case 2:
		printf("Bad minutes!\n");
		return -1;
		break;

	case 3:
		printf("Bad secomds!\n");
		return -1;
		break;

	}

	if ( (hour == 0)&&(min == 0)&&(sec == 0))
	{
		return MIDNIGHT;
	}

	if ( (hour == 12)&&(min == 0)&&(sec == 0))
	{
		return NOON;
	}

	if ( hour < 5)
	{
		return NIGHT;
	}

	if ( hour < 12)
	{
		return MORNING;
	}

	if ( hour < 17)
	{
		return AFTERNOON;
	}

	if ( hour < 22)
	{
		return EVENING;
	}

	if ( hour < 24)
	{
		return NIGHT;
	}

	return -1;
}