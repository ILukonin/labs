// task_2.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdio.h"
#include "test.h"
#include "input.h"


int main()
{
	enum DAYTIME { MORNING, AFTERNOON, EVENING, NIGHT, NOON, MIDNIGHT };

	int hour = 0;
	int min = 0;
	int sec = 0;
	

	if ( gettime("Input time. Format HH:MM:SS\n", &hour, &min, &sec) != 0 )
	{
		printf("Invalid time!\n");
		return 1;
	}

	switch ( test(hour,min,sec) )
	{
	default:
		printf("Somthing wrong!\n");
		return 2;
		break;

	case MORNING:
		printf("Good Morning!\n");
		break;

	case NOON:
		printf("Now is Noon!\n");
		break;

	case AFTERNOON:
		printf("Good Afternoon!\n");
		break;

	case EVENING:
		printf("Good Evening\n");
		break;
	
	case NIGHT:
		printf("Good Night\n");
		break;

	case MIDNIGHT:
		printf("Now is Midnight!\n");
		break;
	}


	printf("time %02d:%02d:%02d\n", hour,min,sec);

	return 0;
}

